Instructions of using git.

1. clone to your local laptop dir:
git clone <Remote URL>
git clone https://bitbucket.org/chutianwen/fedcentricsoftwareworkspace

2. see the branches it have:
git branches

3. switch to a branch you gonna develop and make changes:
git checkout <branch name>

4. if you want to make a new feature and wanna make a new branch of it:
git checkout -b <branch name>

5. for commit and push your branch (supposed you are in your desired branch)
git add .
git commit -m "message"
git push 

6. If you want to merge your "fully developed branch" to master or other branch
git checkout <target branch> (say I want to merge "Jordan" branch to master branch)
git checkout master
git merge Jordan
(after you merge Jordan branch to master branch in local repo, you can commit and push to 
update master branch into bitbucket. Be sure this is a robust branch merge)

7. For updating your local repo to the remote repo on bitbucket
git fetch (this will update all your remote tracking branches)
git checkout <target branch> 
git merge <remote/branch> 
(remote/branch should the one you want to merge into your current branch)
Try not using git pull, since this all mess up your current branch, it will merge all the 
branches changes into your branch.

8. To checkout all the remote branches
git branch -r 

9. To checkout all the remote repos
git remote -v