#!/usr/bin/env bash
driver_memory=10g
driver_cores=1
total_executor_cores=96
executor_cores=2
executor_memory=25g
INPUT=/home/tchu/Desktop/GenomicGraph/NODE
OUTPUT=/sanssd/TestData
/opt/spark-2.0/bin/spark-submit \
     --name "SparkApp" \
     --class "runApp" \
     --master spark://marvin1:7077 \
     --deploy-mode "client" \
     --driver-memory $driver_memory \
     --driver-cores $driver_cores \
     --total-executor-cores $total_executor_cores \
     --executor-cores $executor_cores \
     --executor-memory $executor_memory \
     --conf spark.driver.maxResultSize=2G \
     --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
     --conf spark.kryoserializer.buffer.max=64m \
     --packages graphframes:graphframes:0.2.0-spark2.0-s_2.11 \
     --verbose \
     target/scala-2.11/sparkapp_2.11-1.0.jar \
     $INTPUT $OUTPUT