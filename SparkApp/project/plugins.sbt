logLevel := Level.Warn
addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.6.0")
addSbtPlugin("com.artima.supersafe" % "sbtplugin" % "1.1.0")

// add assembly plugin for creating fat jar file across cluster
//addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2")
