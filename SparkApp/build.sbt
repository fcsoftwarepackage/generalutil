import sbt.Keys._
import sbt._


// define the common properties for all the projects
lazy val commonSettings = Seq(
    organization := "org.fedcentric",
    version := "1.0",
    scalaVersion := "2.11.8"
)

// https://mvnrepository.com/artifact/org.apache.spark/spark-core_2.10
val lib_SparkCore = "org.apache.spark" % "spark-core_2.11" % "2.0.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-sql_2.10
val lib_SparkSQL = "org.apache.spark" % "spark-sql_2.11" % "2.0.0"

val libScalaTest = "org.scalatest" %% "scalatest" % "2.0" % "test"

val libMySQLConnector = "mysql" % "mysql-connector-java" % "5.1.6"

val resolversNeo4jConnector = "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven"
val libNeo4jConnector = "neo4j-contrib" % "neo4j-spark-connector" % "2.0.0-M2"

//val libScalaTest = "org.scalactic" %% "scalactic" % "3.0.0"
//val libScalaTest_test = "org.scalatest" %% "scalatest" % "3.0.0" % "test"

/*
val sharedMergeStrategy: (String => MergeStrategy) => String => MergeStrategy =
    old => {
        case x if x.startsWith("META-INF/ECLIPSEF.RSA") => MergeStrategy.last
        case x if x.startsWith("META-INF/mailcap") => MergeStrategy.last
        case x if x.endsWith("plugin.properties") => MergeStrategy.last
        case x => old(x)
    }

// Load Assembly Settings
assemblySettings

// Assembly App

mainClass in assembly := Some("com.github.ezhulenev.spark.RunSparkApp")

jarName in assembly := "spark-testing-example-app.jar"

mergeStrategy in assembly <<= (mergeStrategy in assembly)(sharedMergeStrategy)
*/

/**
 * Project of application based on spark and graph lib.
 * the val name is the projectID, in file(XXX), XXX is the path
 * lazy val root = (project in file(".")).
 */
lazy val SparkApp = (project in file(".")).
    settings(commonSettings: _*).
    settings(
        name := "SparkApp",
        // libraryDependencies: SettingKey[Seq[ModuleID]]
        libraryDependencies ++= Seq(lib_SparkCore, lib_SparkSQL, libScalaTest, libMySQLConnector)//, libScalaTest_test)
    ).
    settings(
        mainClass in (Compile, run ) := Some("runApp")
    ).
    settings(
        mainClass in (Test, run) := Some("org.fedcentric.SparkApp.tests.Neo4j.runTest")
    ).
    settings(
        resolvers += resolversNeo4jConnector,
        libraryDependencies += libNeo4jConnector
    )


//    settings(
//        resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
//    )

