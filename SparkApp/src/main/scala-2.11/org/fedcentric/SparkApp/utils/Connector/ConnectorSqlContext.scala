package org.fedcentric.SparkApp.utils.Connector

import org.apache.spark.sql.DataFrame

trait ConnectorSqlContext extends Connector[DataFrame]{

    protected def getDataFromSqlContext: DataFrame= {
        spark.sqlContext.read.load(input)
    }
}
