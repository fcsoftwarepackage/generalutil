package org.fedcentric.SparkApp.utils.Logger

import org.apache.log4j.LogManager

class LoggerLog4j(val loggerName:String) extends Logger[org.apache.log4j.Logger]{

    @transient protected val logger = LogManager.getLogger(loggerName)

    /**
     * Logs the message at the info level.
     * @param message to log.
     */
    protected def log(message : String) : Unit = {
        logger.info(message)
    }
    /**
     * Logs the message at the given level.
     * If that level doesn't exist, creates a new level with the given name and priority,
     * and then logs the message at that new level.
     * @param message to log
     * @param level to log the message at
     * @param priority to create custom log level at, if necessary.
     */
    override def log(message : String, level : String, priority : Int = 0) : Unit = {
        //logger.log(Level.forName(level, priority), message)
        //logger.log(Level.(level, priority), message)
    }

    /**
     * Logs the message at the info level.
     * @param message
     */
    override def info(message: String): Unit = {
        logger.info(message)
    }

    /**
     * Logs the message at the warning level.
     * @param message
     */
    override def warning(message: String): Unit = {
        logger.warn(message)
    }

    /**
     * Logs the message at the error level.
     * @param message
     */
    override def error(message: String): Unit = {
        logger.error(message)
    }

    /**
     * Logs the message at the fatal level.
     * @param message
     */
    override def fatal(message: String): Unit = {
        logger.fatal(message)
    }
}

object LoggerLog4j{

//    LoggerLog4j.getClass
    def apply(loggerName : String = "myLogger") = new LoggerLog4j(loggerName)
}