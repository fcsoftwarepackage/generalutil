package org.fedcentric.SparkApp.utils.Logger

import java.io.{File, FileOutputStream, PrintWriter}

class LoggerTextFile(logfileName : String) extends Logger[PrintWriter]{

    lazy val logger = new PrintWriter(new FileOutputStream(new File(logfileName),true))

    /**
     * Logs the given message to the file.
     * @param msg to log
     */
    protected def log(msg : String) : Unit = {
        logger.print(msg)
        logger.flush()
    }

    /**
     * Logs the message at the given level, ignoring priority entirely.
     * Writes the string "[$level] : $message" to the file.
     *
     * @param message  to log
     * @param level    to log the message at
     * @param priority ignored
     */
    protected override def log(message: String, level: String, priority: Int = 0): Unit = {
        try{
            if (level != null)
                log("[%s]:%s".format(level, message))
            else
                throw new RuntimeException("Log Level is not given")
        }catch{
            case e:Exception => error("The exception is: %s".format(e.getMessage))
        }finally{
            println("Wrong log invoke")
        }
    }

    /**
     * Logs the message at the info level.
     * Prints "[INFO] : $message".
     *
     * @param message
     */
    override def info(message: String): Unit = {
        log(message, "INFO")
    }

    /**
     * Logs the message at the wraning level.
     * Prints "[WARN] : $message".
     *
     * @param message
     */
    override def warning(message: String): Unit = {
        log(message, "WARN")
    }

    /**
     * Logs the message at the error level.
     * Prints "[ERROR] : $message".
     *
     * @param message
     */
    override def error(message: String): Unit = {
        log(message, "ERROR")
    }

    /**
     * Logs the message at the fatal level.
     * Prints "[FATAL] : $message".
     *
     * @param message
     */
    override def fatal(message: String): Unit = {
        log(message, "FATAL")
    }
}