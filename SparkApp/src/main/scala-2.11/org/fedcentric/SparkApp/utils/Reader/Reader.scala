package org.fedcentric.SparkApp.utils.Reader


/**
 * Tool Reader take cares of input path
 * Process given paths from HDFS or OS Disk
 * Set +T to for selecting different ReadType, and constructor of Reader[Any]
 */
trait Reader[+T] {


    /**
     * Interface to the main app.
     * Given a user defined input path URL, return an extracted list of all the
     * valid paths.
     * @param Mode_Read:
     *                 @0: Return one element list, all the files are joined with ","
     *                 @other: Return all elements in a list, spark will process iteratively.
     *
     * @return
     */
    def readInputPath(path:String, Mode_Read:Int, filter: T => Boolean): List[String]

    /**
     * Detect the readerType based on the given input path
     * @param path User input path
     * @return boolean variable indicating the type of read source, from HDFS, or local disk, or something else.
     */
    def detect(path : String) : Boolean
}

