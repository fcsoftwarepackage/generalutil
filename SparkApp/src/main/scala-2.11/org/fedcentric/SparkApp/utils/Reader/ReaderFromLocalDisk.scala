package org.fedcentric.SparkApp.utils.Reader

import java.io.File
import scala.collection.mutable.ListBuffer

trait ReaderFromLocalDisk extends Reader[File]{

//    def t = classTag[File]
    /**
     * Read all the valid paths from OS Disk, then apply self-defined filter to
     * get desired data, prepare the results in a list.
     * @param mode Choosing to process all the files as a whole or iteratively
     * @param selfFilter Self-defined rule to get the desired data
     * @tparam T Generic type for defining self-filter
     * @return List of valid paths as String
     */
    def readInputPath(path:String, mode:Int, selfFilter: File => Boolean): List[String] = {
        mode match{
            case 0 => List(getFilteredDiskPaths(path, selfFilter).mkString(","))
            case _ => getFilteredDiskPaths(path, selfFilter)
        }
    }

    /**
     * Detects if a given pathname is a valid hdfs path
     * @param path Name of the path to test.
     * @return If the string contains the substring "hfds" or "ihdfs".
     */
    def detect(path : String): Boolean = {
        // TODO(schneiderj) rewrite this regex to produce fewer false positives.
        !path.matches(".*(?i)hdfs.*")
    }

    /**
     * Extract all filtered OS Disk paths
     * @param selfFilter Self-defined rule to get the desired data
     * @tparam T Generic type for defining self-filter
     * @return List of desired paths
     */
    protected def getFilteredDiskPaths(path:String, selfFilter: File => Boolean): List[String] = {
        val inputPaths = path.split(",")
        // check if the input contains multiple file including ","
        val validPaths = inputPaths.map(new File(_)).filter(_.exists())
        val validFilePaths = validPaths.filter(_.isFile).map(_.getPath).toList
        val validDirPaths = validPaths.filter(_.isDirectory)

        val filteredPaths = new ListBuffer[String]()
        filteredPaths.appendAll(validFilePaths)

        // Iterate all the valid dir path and append all valid file paths
        for(validDirPath <- validDirPaths){
            val allDiskPaths = getAllDiskPaths(validDirPath)
            val allDiskPaths_filtered = allDiskPaths.filter(selfFilter).map(_.getPath)
            filteredPaths.appendAll(allDiskPaths_filtered)
        }
        filteredPaths.toList
    }

    /**
     * Find all the directory tree given one OS disk path
     * @param DirName OS Disk path
     * @return List of OS Disk paths as current directory tree
     */
    protected def getAllDiskPaths(DirName: File): List[File] = {

        // return the directory tree, root is current Dir
        def recursiveListFiles(f: File): Array[File] = {
            val files = f.listFiles
            files ++ files.filter(_.isDirectory).flatMap(recursiveListFiles)
        }
        val allPaths = recursiveListFiles(DirName).toList
        allPaths
    }
}

object ReaderFromLocalDisk{

    def apply():ReaderFromLocalDisk = new ReaderFromLocalDiskImpl()
    private class ReaderFromLocalDiskImpl() extends ReaderFromLocalDisk {

    }
}

