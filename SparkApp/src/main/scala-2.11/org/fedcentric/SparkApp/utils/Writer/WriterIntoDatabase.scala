package org.fedcentric.SparkApp.utils.Writer

import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SaveMode._

import scala.reflect.ClassTag


trait WriterIntoDatabase extends Writer{

    override def writeData[T:ClassTag,S:ClassTag](Input:T, Output:S, NumPartitions:Int = 0, Format:String = "parquet", Mode:SaveMode = Overwrite):Unit = {}

}
