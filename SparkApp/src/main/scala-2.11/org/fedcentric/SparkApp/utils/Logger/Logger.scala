package org.fedcentric.SparkApp.utils.Logger

trait Logger[T] extends Serializable{

    protected val logger : T

    private val NanoToSecond = scala.math.pow(10, -9)

    /**
     * Times the given block of code in seconds.
     *
     * TODO(joschnei) : Time is notoriously hard and I think nanoTime has a lot of problems in particular.
     * Look into this.
     *
     * @param block of code to run
     * @tparam R
     * @return time the block took to run in seconds.
     */
    private def time[R](block: => R): Double = {
        val t0 = System.nanoTime()
        block    // call-by-name
        val t1 = System.nanoTime()
        (t1 - t0) * NanoToSecond
    }

    def benchmark[R](block: => R): Unit = {
        log(time{block}.toString)
    }

    /**
     * Logs a message. If the logger enforces levels, will be enforced at the INFO level, or equivalent default.
     * @param message to log.
     */
    protected def log(message : String) : Unit

    /**
     * Logs a given message at a given level, if possible.
     * Specifically, if the underlying logger has no support for levels,
     * the message will be prepended with "[$level] : ".
     * If the logger has support for levels, it will try to log at the given level. If the logger supports levels,
     * but the given level does exist, the logger will try to create a custom level
     * with the given name and priority. If there is no support for custom log levels,
     * then the logger will log it at the default level.
     *
     * TODO(joschnei) : Maybe we want to throw an error if the log level doesn't exist?
     *
     * @param message to log
     * @param level to log the message at
     * @param priority to create custom log level at, if necessary.
     */
    protected def log( message : String, level : String, priority : Int = 0) : Unit

    /**
     * Logs the message at the info level, if supported.
     * @param message
     */
    protected def info(message : String) : Unit

    /**
     * Logs the message at the warning level, if supported.
     * @param message
     */
    protected def warning(message : String) : Unit

    /**
     * Logs the message at the error level, if supported.
     * @param message
     */
    protected def error(message : String) : Unit

    /**
     * Logs the message at the fatal level, if supported.
     * @param message
     */
    protected def fatal(message : String) : Unit
}