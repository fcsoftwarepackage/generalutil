package org.fedcentric.SparkApp.utils.Connector

import org.apache.spark.rdd.RDD

trait ConnectorSparkContext extends Connector[RDD[String]]{

    protected def getDataFromSparkContext:RDD[String] = {
        spark.sparkContext.textFile(input)
    }
}
