package org.fedcentric.SparkApp.utils.Writer

import org.apache.spark.sql.SaveMode._
import org.apache.spark.sql._

import scala.reflect._


trait Writer{

    def writeData[T:ClassTag,S:ClassTag](Input:T, Output:S, NumPartitions:Int, Format:String = "parquet", Mode:SaveMode = Overwrite):Unit

}
