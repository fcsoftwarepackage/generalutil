package org.fedcentric.SparkApp.utils.Connector

import org.apache.spark.sql.SparkSession


trait Connector[T] {

    /**
     * Source path as input, can be different source
     */
    protected var input:String

    /**
     * Interface to the main app.
     * Retrieved data from different types connectors given input
     * @return
     */
    protected def getData:T

    /**
     *
     */
    protected val spark: SparkSession
}
