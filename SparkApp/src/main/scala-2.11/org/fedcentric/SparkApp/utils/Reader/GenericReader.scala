package org.fedcentric.SparkApp.utils.Reader

import scala.beans.BeanProperty
import scala.reflect._

trait GenericReader {

    @BeanProperty
    var inputPath: String = _

    def readPath[T: ClassTag](mode : Int, filter : T => Boolean) : List[String]

    protected def selectTypeReader[T: ClassTag]() : Reader[T]
}

object GenericReader {

    /**
     * Registering all types of readers to a geneticReader
     * @param readerTypes
     * @return
     */
    def apply[T: ClassTag](TypeReaders: Reader[T]*): GenericReader = new GenericReaderImpl(TypeReaders)

    /**
     * A helper class which implements the trait GenericReader
     * @param TypeReaders
     */
    private class GenericReaderImpl(private val TypeReaders: Seq[Reader[Any]]) extends GenericReader {
        /**
         * Taken the mode and filter given by user, select the correct type reader, then return
         * a list of valid paths as Strings.
         * @param mode
         * @param filter
         * @tparam T
         * @return
         */
        def readPath[T: ClassTag](mode: Int, filter: T => Boolean): List[String] = {
            val currentReader: Reader[T] = selectTypeReader[T]()
            if(currentReader != null) {
                currentReader.readInputPath(inputPath, mode, filter)
            }else{
                Nil
            }
        }

        /**
         * selecting the correct type reader by:
         * a). checking the input pattern to which it belongs, HDFS, local disk or something else
         * b). checking the filtered type readers match the user-defined filter
         *
         * Notice, the program will default choose the first type reader in the valid candidates,
         * please follow the rule when registering type readers.
         * @tparam T Types of Reader, HDFS(Path), Local Disk(File) or something else
         * @return The correct type of Reader
         */
        def selectTypeReader[T: ClassTag](): Reader[T] = {

            try {
                // Figure out the problem:
                //abstract type pattern is unchecked since it is eliminated by erasure
                // http://stackoverflow.com/questions/18136313/abstract-type-pattern-is-unchecked-since-it-is-eliminated-by-erasure
                TypeReaders.filter(_.detect(inputPath)).head.asInstanceOf[Reader[T]]
            }
            catch{
                // .head works on an empty Collection, no valid type readers
                case ex: NoSuchElementException => ex.printStackTrace(); null
            }
        }
    }
}

