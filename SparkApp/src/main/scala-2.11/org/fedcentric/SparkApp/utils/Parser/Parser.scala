package org.fedcentric.SparkApp.utils.Parser

/**
 *
 * @tparam T Input data type, generated from Connector
 * @tparam S Output data type
 */
trait Parser[T, S] {

    /**
     * Retrieved data from different types connectors given input
     */
    protected val input:T

    /**
     * Interface to the main app.
     *
     * @return
     */
    protected def parseData:S
}
