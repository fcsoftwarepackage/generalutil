
package object utils {
    object repartitionLevel extends Enumeration {
        type repartitionLevel = Value
        val light, medium, strong = Value
    }

}
