package org.fedcentric.SparkApp.utils.Writer

import java.io.File

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SaveMode._
import org.apache.spark.sql._
import utils.repartitionLevel
import utils.repartitionLevel._

import scala.reflect._
import scala.math.floor
trait WriterIntoFileSystem extends Writer{

    /**
     * Consolidate number of small parquet files into fewer larger parquet files,
     * This will help with reloading IO speed. Will use coalesce method.
     * Provide different level of consolidating.
     *
     * @param InputPath
     * @param OutputPath
     * @param level
     */
    def rePartitionStorage(InputPath:String, OutputPath:String, format:String, level:repartitionLevel, spark:SparkSession):Unit = {
        val InputPathDir = new File(InputPath)
        var numConsolidateFiles:Int = 0
        val (totalNumFiles, averageFileSize) = getAverageFileSize(InputPathDir, format.toLowerCase)
        println("totalNumFiles:%f, averageFileSize:%f".format(totalNumFiles, averageFileSize))
        var coalesceCC = 0.toDouble
        level match{
            // each block size is 32 mb
            case repartitionLevel.light => {
                coalesceCC = 32 / averageFileSize
            }
            // each block size is 64 mb
            case repartitionLevel.medium => {
                coalesceCC = 64 / averageFileSize
            }
            // each block size is 128 mb
            case repartitionLevel.strong => {
                coalesceCC = 128 / averageFileSize
            }
        }
        numConsolidateFiles = floor(totalNumFiles/coalesceCC).toInt
//        numConsolidateFiles = if(numConsolidateFiles == 0) 1 else numConsolidateFiles
        val df = spark.sqlContext.read.load(InputPath)
        writeData(df, OutputPath, numConsolidateFiles)
    }


    override def writeData[T:ClassTag,S:ClassTag](Input:T, Output:S, NumPartitions:Int = 0, Format:String = "parquet", Mode:SaveMode = Overwrite):Unit = {
        Input match{
            case _ if classTag[T] == classTag[DataFrame] => {
                Output match{
                    case _ if classTag[S] == classTag[String] => {
                        println("Dataframe - String")
                        if (NumPartitions == 0)
                            Input.asInstanceOf[DataFrame].write.format(Format).mode(Mode).save(Output.asInstanceOf[String])
                        else
                            Input.asInstanceOf[DataFrame].coalesce(NumPartitions).write.format(Format).mode(Mode).save(Output.asInstanceOf[String])
                    }
                    case _ => {
                        println("Not supported output format")
                    }
                }
            }
            case _ if classTag[T] == classTag[RDD[_]] => {
                Output match{
                    case _ if classTag[S] == classTag[String] => {
                        println("RDD[_] - String")
                        if (NumPartitions == 0)
                            Input.asInstanceOf[RDD[_]].saveAsTextFile(Output.asInstanceOf[String])
                        else
                            Input.asInstanceOf[RDD[_]].coalesce(NumPartitions).saveAsTextFile(Output.asInstanceOf[String])
                    }
                    case _ => {
                        println("Not supported output format")
                    }
                }
            }
            case _ => println("Not supported input format")
        }
    }

    /**
     * Calculate average size of file in the given directory
     * @param dir Given directory
     * @return average size of file, in megaBytes.
     */
    private def getAverageFileSize(dir:File, format:String):(Double, Double) = {

        def recursiveListFiles(f: File): Array[File] = {
            val files = f.listFiles
            files ++ files.filter(_.isDirectory).flatMap(recursiveListFiles)
        }

        // filter out all the directories and only calculate the files with given format
        val allFilePaths = recursiveListFiles(dir).filter(_.isFile).filter( x => {
            x.getName.split('.').last == format
        })
        val totalNumFiles = allFilePaths.length.toDouble
        val totalSize = allFilePaths.map(_.length()).sum.toDouble
        val averageSize = totalSize/totalNumFiles

        // converting bytes into megabytes
        (totalNumFiles, averageSize/1024/1024)
    }
}
