package org.fedcentric.SparkApp.utils.Reader
import org.apache.spark.sql.SparkSession
import org.apache.hadoop.fs.Path
import scala.collection.mutable.ListBuffer

trait ReaderFromHDFS extends Reader[Path]{

//    def t = classTag[Path]

    protected val spark: SparkSession
    private lazy val conf = spark.sparkContext.hadoopConfiguration
    private lazy val fs = org.apache.hadoop.fs.FileSystem.get(conf)

    /**
     * Read all the valid paths from HDFS, then apply self-defined filter to
     * get desired data, prepare the results in a list.
     * @param mode Choosing to process all the files as a whole or iteratively
     * @param selfFilter Self-defined rule to get the desired data
     * @tparam T Generic type for defining self-filter
     * @return List of valid paths as String
     */
    def readInputPath(path:String, mode:Int, selfFilter: Path => Boolean): List[String] = {
        mode match{
            case 0 => List(getFilteredHDFSPaths(path, selfFilter).mkString(","))
            case _ => getFilteredHDFSPaths(path, selfFilter)
        }
    }

    /**
     * Detects if a given pathname is a valid hdfs path
     * @param path Name of the path to test.
     * @return If the string contains the substring "hfds" or "ihdfs".
     */
    def detect(path : String): Boolean = {
        // TODO(schneiderj) rewrite this regex to produce fewer false positives.
        path.matches(".*(?i)hdfs.*")
    }

    /**
     * Extract all filtered hdfs paths
     * @param selfFilter Self-defined rule to get the desired data
     * @tparam T Generic type for defining self-filter
     * @return List of desired paths
     */
    protected def getFilteredHDFSPaths(path:String, selfFilter: Path => Boolean): List[String] ={
        val inputPaths = path.split(",")
        val validPaths = inputPaths.map(new Path(_)).filter(fs.exists)
        val validFilePaths = validPaths.filter(fs.isFile).map(_.toString).toList
        val validDirPaths = validPaths.filter(fs.isDirectory)
        // Append all valid file paths (non-directory)
        val filteredPaths = new ListBuffer[String]()
        filteredPaths.appendAll(validFilePaths)
        // Iterate all the valid dir path and append all valid file paths
        for(validDirPath <- validDirPaths){
            val allHDFSPaths = getAllHDFSPaths(validDirPath)
            val allHDFSPaths_filtered = allHDFSPaths.filter(selfFilter).map(_.toString)
            filteredPaths.appendAll(allHDFSPaths_filtered)
        }
        filteredPaths.toList
    }
    /**
     * Find all the directory tree given one HDFS path
     * @param DirName HDFS path
     * @return List of HDFS paths as current directory tree
     */
    protected def getAllHDFSPaths(DirName: Path): List[Path] = {
        val allfilesItr = fs.listFiles(DirName, true)
        val allPaths = new ListBuffer[Path]()
        while(allfilesItr.hasNext){
            allPaths.append(allfilesItr.next().getPath)
        }
        allPaths.toList
    }
}

object ReaderFromHDFS{

    def apply(spark: SparkSession):ReaderFromHDFS = new ReaderFromHDFSImpl(spark)

    private class ReaderFromHDFSImpl(val spark: SparkSession) extends ReaderFromHDFS {

    }
}
