import java.io.File

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.fedcentric.SparkApp.utils.Logger.LoggerLog4j
import org.fedcentric.SparkApp.utils.Reader.{GenericReader, ReaderFromHDFS, ReaderFromLocalDisk}
import org.fedcentric.SparkApp.utils.Writer.WriterIntoFileSystem
import utils.repartitionLevel._

object runApp{

    val logger = LoggerLog4j("LoggerMySQL")

    def selfFilter(file:File): Boolean ={
        file.getName.startsWith("r")
    }

    def main(args: Array[String]): Unit = {

        println("-------------Begin------------")
        println("Total input paras: %d".format(args.length))
        val conf_cluster = new SparkConf()
        val spark = SparkSession.builder().config(conf_cluster).getOrCreate()
        val inputPath = "/home/tchu/Desktop/GenomicGraph/NODE"
        val genericReader = GenericReader(ReaderFromLocalDisk(), ReaderFromHDFS(spark))
        genericReader.setInputPath(inputPath)

        val mode = 1
        val outputPaths = genericReader.readPath(mode, selfFilter)
        logger.info("Listing all the paths")
        for(outputPath <- outputPaths){
            println(outputPath)
            logger.info(outputPath)
        }

        val writer = new WriterIntoFileSystem{}
        val InputPath = args(0)
        val OutputPath = args(1)
        println("Input: %s\nOutput: %s".format(InputPath, OutputPath))
        writer.rePartitionStorage(InputPath, OutputPath, "parquet", strong, spark)

        spark.stop()
        logger.info("Job done")
        println("-------------Stop------------")
    }
}
