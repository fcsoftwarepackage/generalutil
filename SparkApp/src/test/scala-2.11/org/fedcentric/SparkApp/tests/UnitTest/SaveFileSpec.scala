package org.fedcentric.SparkApp.tests.UnitTest

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession


class SaveFileSpec extends UnitSpec {

    "The file" should "save into c:/Users/Tianwen/Desktop" in {
        val conf_cluster = new SparkConf()
        val spark = SparkSession.builder().config(conf_cluster).getOrCreate()

        val input = spark.sparkContext.textFile("c:/Users/Tianwen/Desktop/KitchenBath.txt")
        input.saveAsTextFile("c:/Users/Tianwen/Desktop/Test")
        spark.stop()
    }
}
