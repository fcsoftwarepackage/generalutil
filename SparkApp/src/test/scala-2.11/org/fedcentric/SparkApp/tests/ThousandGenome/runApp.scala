import java.io.File

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.fedcentric.SparkApp.utils.Logger.LoggerLog4j
import org.fedcentric.SparkApp.utils.Reader.{GenericReader, ReaderFromHDFS, ReaderFromLocalDisk}


object runApp{

    val logger = LoggerLog4j("LoggerMySQL")

    def selfFilter(file:File): Boolean ={
        file.getName.startsWith("r")
    }

    def main(args: Array[String]): Unit = {

        // if hadoop home is not set, set it the path which includes winutils.
        System.setProperty("hadoop.home.dir", "C:\\Users\\Tianwen\\Desktop\\FedCentric\\Softwares\\HDFS")
        println("-------------Begin------------")
        val conf_cluster = new SparkConf()
        val spark = SparkSession.builder().config(conf_cluster).getOrCreate()
        val inputPath = "C:\\Users\\Tianwen\\Desktop\\FedCentric\\BitBucket\\FedCentricSoftwareWorkSpace\\SparkApp\\src"
        //        val inputPath = "/home/tchu"
        val genericReader = GenericReader(ReaderFromLocalDisk(), ReaderFromHDFS(spark))
        genericReader.setInputPath(inputPath)

        val mode = 1
        val outputPaths = genericReader.readPath(mode, selfFilter)
        logger.info("Listing all the paths")
        for(outputPath <- outputPaths){
            println(outputPath)
            logger.info(outputPath)
        }
        //        spark.sparkContext.textFile()
        val a = spark.sparkContext.parallelize((0 to 10).toList)
        val r = a.map( x=> {
            logger.info("This is line:%s".format(x))
            x
        })
        r.collect

        val input = spark.sparkContext.textFile("file:///c:/Users/Tianwen/Desktop/KitchenBath.txt")
        println(input.count())
        input.saveAsTextFile("file:///c:/Users/Tianwen/Desktop/Test")
        spark.stop()

        logger.info("Job done")
        println("-------------Stop------------")
    }
}
