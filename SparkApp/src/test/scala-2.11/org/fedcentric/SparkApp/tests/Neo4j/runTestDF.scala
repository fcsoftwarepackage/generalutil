package org.fedcentric.SparkApp.tests.Neo4j

import org.fedcentric.SparkApp.tests.FlatTest.ConfiguredSparkFlatSpec
import org.neo4j.spark._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

object runTestDF extends ConfiguredSparkFlatSpec{

    val df = Neo4jDataFrame.withDataType(spark.sqlContext, "MATCH (n) return id(n) as id",Seq.empty, "id" -> LongType)
    // df: org.apache.spark.sql.DataFrame = [id: bigint]

    df.count
    // res0: Long = 1000000


    val query = "MATCH (n:Person) return n.age as age"
    val df = Neo4jDataFrame.withDataType(spark.sqlContext,query, Seq.empty, "age" -> LongType)
    // df: org.apache.spark.sql.DataFrame = [age: bigint]
    df.agg(sum(df.col("age"))).collect()
    // res31: Array[org.apache.spark.sql.Row] = Array([49500000])

    query: String = MATCH (n:Person) return n.age as age

    // val query = "MATCH (n:Person)-[:KNOWS]->(m:Person) where n.id = {x} return m.age as age"
    val query = "MATCH (n:Person) where n.id = {x} return n.age as age"
    val rdd = sc.makeRDD(1.to(1000000))
    val ages = rdd.map( i => {
        val df = Neo4jDataFrame.withDataType(spark.sqlContext,query, Seq("x"->i.asInstanceOf[AnyRef]), "age" -> LongType)
        df.agg(sum(df("age"))).first().getLong(0)
    })
    // TODO
    val ages.reduce( _ + _ )

    val df = Neo4jDataFrame(sqlContext, "MATCH (n) WHERE id(n) < {maxId} return n.name as name",Seq("maxId" -> 100000),"name" -> "string")
    df.count
    // res0: Long = 100000
}
