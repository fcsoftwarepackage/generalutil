package org.fedcentric.SparkApp.tests.UnitTest

import java.sql.{Connection, DriverManager}


class MySQLSpec extends UnitSpec {

    val url = "jdbc:mysql://localhost:3306/mysql"
    val driver = "com.mysql.jdbc.Driver"
    val username = "root"
    val password = ""
    var connection: Connection = _

    "MySQL Connector" should "connected" in {
        try {
            Class.forName(driver)
            connection = DriverManager.getConnection(url, username, password)
            val statement = connection.createStatement
            //val rs0 = statement.executeQuery("use carpooling")
            val rs = statement.executeQuery("select firstname, lastname from Persons")
            while (rs.next) {
                val fn = rs.getString("firstname")
                val ln = rs.getString("lastname")
                println("host = %s, user = %s".format(fn, ln))
            }
        } catch {
            case e: Exception => e.printStackTrace
        }
        connection.close
    }
}