package org.fedcentric.SparkApp.tests.FlatTest

import org.apache.log4j.LogManager
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfterAll, FlatSpec}

trait ConfiguredSparkFlatSpec extends FlatSpec with BeforeAndAfterAll {
    private val log = LogManager.getLogger(classOf[ConfiguredSparkFlatSpec])

    private lazy val sparkConf = {
        val master = "spark.master"

        log.info(s"Create spark context. Master: $master")
        val assembledTests = sys.props.get("ASSEMBLED_TESTS")
        val baseConf = new SparkConf().setMaster(master).setAppName("SparkTestingExample")

        assembledTests match {
            case None =>
                log.warn(s"Assembled tests jar not found. Standalone Spark mode is not supported")
                baseConf
            case Some(path) =>
                log.info(s"Add assembled tests to Spark Context from: $path")
                baseConf.setJars(path :: Nil)
        }
    }
    val spark = SparkSession.builder().config(sparkConf).getOrCreate()
    lazy val sc = spark.sparkContext

    override protected def afterAll(): Unit = {
        super.afterAll()
        sc.stop()
    }
}
