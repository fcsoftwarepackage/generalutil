package org.fedcentric.SparkApp.tests.Neo4j

import java.io.File

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.fedcentric.SparkApp.utils.Logger.LoggerLog4j
import org.fedcentric.SparkApp.utils.Reader.{GenericReader, ReaderFromHDFS, ReaderFromLocalDisk}


object runTest{

    val logger = LoggerLog4j("LoggerMySQL")

    def selfFilter(file:File): Boolean ={
        file.getName.startsWith("r")
    }

    def main(args: Array[String]): Unit = {

        println("-------------Test1 Begin------------")
        System.setProperty("hadoop.home.dir", "C:\\Users\\Tianwen\\Desktop\\FedCentric\\Softwares\\HDFS")

        val conf_cluster = new SparkConf()
        val spark = SparkSession.builder().config(conf_cluster).getOrCreate()
        //        val inputPath = "C:\\Users\\Tianwen\\Desktop\\FedCentric\\BitBucket\\FedCentricSoftwareWorkSpace\\SparkApp\\src"
        val inputPath = "/home/tchu"
        val genericReader = GenericReader(ReaderFromLocalDisk(), ReaderFromHDFS(spark))
        genericReader.setInputPath(inputPath)

        val mode = 1
        val outputPaths = genericReader.readPath(mode, selfFilter)
        logger.info("Listing all the paths")
        for(outputPath <- outputPaths){
            println(outputPath)
            logger.info(outputPath)
        }
        //spark.sparkContext.textFile()
        spark.stop()
        logger.info("Job done")
        println("-------------Stop------------")

    }
}

