package org.fedcentric.SparkApp.tests.UnitTest



class EnumerationSpec extends UnitSpec{
    "Enumeration" should "print" in {
        object test extends Enumeration {
            type test = Value
            val light, medium, strong = Value
        }
        import test._
        def fun(x:test) = {
            x match{
                // each block size is 32 mb
                case test.light => {
                    println(1)
                }
                // each block size is 64 mb
                case test.medium => {
                    println(2)
                }
                // each block size is 128 mb
                case test.strong => {
                    println(3)
                }
            }
        }
        fun(medium)
    }
}
