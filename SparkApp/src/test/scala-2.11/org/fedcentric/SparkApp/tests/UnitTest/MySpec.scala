package org.fedcentric.SparkApp.tests.UnitTest

import java.io.File

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.fedcentric.SparkApp.utils.Reader.{GenericReader, ReaderFromHDFS, ReaderFromLocalDisk}

class MySpec extends UnitSpec{

	def selfFilter(file:File): Boolean ={
		file.getName.startsWith("r")
	}

	"My util" should "detect specific readerModule from modules" in{
		println("-------------Begin------------")
		val conf_cluster = new SparkConf()
		val spark = SparkSession.builder().config(conf_cluster).getOrCreate()
		val inputPath = "C:\\Users\\Tianwen\\Desktop\\FedCentric\\BitBucket\\FedCentricSoftwareWorkSpace\\SparkApp\\src"

		val genericReader = GenericReader(ReaderFromLocalDisk(), ReaderFromHDFS(spark))
		genericReader.setInputPath(inputPath)

		val mode = 1
		val outputPaths = genericReader.readPath(mode, selfFilter)

		for (outputPath <- outputPaths) {
			println(outputPath)
		}

		//        spark.sparkContext.textFile()
		spark.stop()
		println("-------------Stop------------")
	}
}
